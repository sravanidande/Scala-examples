package seartipy.starter.suites

import seartipy.starter.Complex
import seartipy.starter.Rational

import org.scalatest.{FunSuite, Matchers}

import org.scalatest._

class ComplexSuite extends FunSuite with Matchers {
  test("constructor"){
    val r = new Complex(new Rational(1,2),new Rational(2,3))
    assert(r.real.numerator === 1)
    assert(r.real.denominator === 2)
    assert(r.imaginary.numerator === 2)
    assert(r.imaginary.denominator === 3)
  }

  test("add"){
    val r = new Complex(new Rational(1,2),new Rational(2,3))
    val r1 = new Complex(new Rational(1,4),new Rational(1,2))
    val r3 = r+r1
    assert(r3.real.numerator === 3)
    assert(r3.real.denominator === 4)
    assert(r3.imaginary.numerator === 7)
    assert(r3.imaginary.denominator === 6)
  }

  test("sub"){
    val r = new Complex(new Rational(3,2),new Rational(1,2))
    val r1 = new Complex(new Rational(3,4),new Rational(1,4))
    val r2 = r-r1
    assert(r2.real.numerator===3)
    assert(r2.real.denominator === 4)
    assert(r2.imaginary.numerator === 1)
    assert(r2.imaginary.denominator === 4)
  }

  test("mul"){
    val r = new Complex(new Rational(3,2),new Rational(1,2))
    val r1 = new Complex(new Rational(1,2),new Rational(1,4))
    val r2 = r * r1
    assert(r2.real.numerator===3)
    assert(r2.real.denominator === 4)
    assert(r2.imaginary.numerator === 1)
    assert(r2.imaginary.denominator === 8)

  }

//  test("div"){
//    var
//  }

}
