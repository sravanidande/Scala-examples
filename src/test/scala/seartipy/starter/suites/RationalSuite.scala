package seartipy.starter.suites

import seartipy.starter.Rational

import org.scalatest._


class RationalSuite extends FunSuite with Matchers {
  test("constructor"){
    var r = new Rational(1,2)
    assert(r.numerator === 1)
    assert(r.denominator === 2)

    var r2 = new Rational(3, 4)
    assert(r2.numerator === 3)
    assert(r2.denominator === 4)

    var r3 = new Rational(6, 8)
    assert(r3.numerator === 3)
    assert(r3.denominator === 4)
  }

  test("add"){
    var r = new Rational(1,2)
    var r1 = new Rational(3,4)
    var r2 = r + r1
    assert(r2.numerator === 5)
    assert(r2.denominator === 4)

//    var r3 = new Rational(0,1)
//    var r4 = new Rational(0,1)
//    var r5 = r3 + r4
//    assert(r3.numerator === 0)
//    assert(r3.denominator === 0)
  }

  test("sub"){
    val r = new Rational(3,4)
    var r1 = new Rational(1,2)
    var r2 = r-r1
    assert(r2.numerator === 1)
    assert(r2.denominator === 4)

    var r3 = new Rational(3,2)
    var r4 = new Rational(1,4)
    var r5 = r3-r4
    assert(r5.numerator === 5)
    assert(r5.denominator === 4)

  }

  test("mul"){
    var r = new Rational(5,2)
    var r1 = new Rational(2,3)
    var r2 = r*r1
    assert(r2.numerator === 5)
    assert(r2.denominator === 3)
  }

  test("div"){
    var r = new Rational(1,2)
    var r1 = new Rational(3,4)
    var r2 = r/r1
    assert(r2.numerator === 2)
    assert(r2.denominator === 3)
  }


}
