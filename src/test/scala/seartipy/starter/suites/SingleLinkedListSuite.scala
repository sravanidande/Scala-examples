package seartipy.starter.suites

import org.scalatest.{FunSuite, Matchers}
import seartipy.starter.SingleLinkedList

class SingleLinkedListSuite extends FunSuite with Matchers {
  test("add"){
    var node = new SingleLinkedList()
    node.addFirst(10)
    assert(node.size == 1)
    node.addFirst(20)
    node.addFirst(30)
    assert(node.size === 3)
    assert(node.first === 30)
  }

  test("remove") {
    var node = new SingleLinkedList()
    node.addFirst(10)
    node.addFirst(20)
    node.addFirst(30)
    assert(node.size === 3)
    assert(node.removeFirst() === 30)
    assert(node.removeFirst() === 20)
    assert(node.size === 1)
  }

  test("nodeAt") {
    var node = new SingleLinkedList()
    node.addFirst(10)
    node.addFirst(20)
    node.addFirst(30)
    node.addFirst(40)
    assert(node.size === 4)
    assert(node.nodeAt(0).data === 40)
    assert(node.toArray === Array(40,30,20,10))
    assert(node.removeFirst() === 40)
    assert(node.size === 3)
    assert(node.nodeAt(0).data === 30)
    assert(node.nodeAt(2).data === 10)
  }

  test("insertAfter") {
    var node = new SingleLinkedList()
    node.addFirst(10)
    node.addFirst(20)
    node.addFirst(30)
    node.insertAfter(40,node.nodeAt(0))
    node.addFirst(40)
    assert(node.toArray === Array(40,30,40,20,10))
    node.insertAfter(60,node.nodeAt(3))
    assert(node.toArray === Array(40,30,40,20,60,10))
    assert(node.size === 6)
  }

  test("removeAfter") {
    var node = new SingleLinkedList()
    node.addFirst(10)
    node.addFirst(20)
    node.addFirst(30)
    node.removeAfter(node.nodeAt(0))
    assert(node.toArray === Array(30,10))
    node.addFirst(40)
    node.removeAfter(node.nodeAt(1))
    assert(node.toArray === Array(40,30))
  }


}
