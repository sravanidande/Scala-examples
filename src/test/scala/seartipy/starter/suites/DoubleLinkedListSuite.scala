package seartipy.starter.suites

import org.scalatest.{FunSuite, Matchers}
import seartipy.starter.DoubleLinkedList

class DoubleLinkedListSuite extends FunSuite with Matchers {

  test("addfirst") {
    val double = new DoubleLinkedList()
    assert(double.size() === 0)
    double.addFirst(10)
    assert(double.size() === 1)
    assert(double.first() === 10)
    double.addFirst(20)
    assert(double.size() === 2)
    assert(double.first() === 20)
    assert(double.last() === 10)
    double.addFirst(30)
    assert(double.toArray === Array(30,20,10))

  }

  test("addLast"){
    val double = new DoubleLinkedList()
    assert(double.size() === 0)
    double.addLast(10)
    assert(double.size() === 1)
    double.addLast(20)
    assert(double.size() === 2)
    assert(double.first() === 10)
    assert(double.last() === 20)
    double.addLast(30)
    double.addFirst(40)
    assert(double.toArray === Array(40,10,20,30))
  }


  test("removeFirst"){
    var double = new DoubleLinkedList()
    double.addFirst(10)
    double.addFirst(20)
    double.addLast(30)
    double.addLast(40)
    assert(double.toArray === Array(20,10,30,40))
    assert(double.size() === 4)
    assert(double.first() === 20)
    assert( double.removeFirst() === 20)
    assert(double.toArray === Array(10,30,40))
    assert(double.removeFirst() === 10)
    assert(double.size() === 2)
  }

  test("removeLast"){
    var double = new DoubleLinkedList()
    double.addFirst(10)
    double.addFirst(20)
    double.addLast(30)
    double.addLast(40)
    assert(double.first() === 20)
    assert(double.last() === 40)
    assert(double.toArray === Array(20,10,30,40))
    assert(double.size() === 4)
    assert(double.removeLast() === 40)
    assert(double.removeLast() === 30)
    double.addFirst(40)
    double.addLast(50)
    double.addFirst(60)
    assert(double.removeLast() === 50)
    assert(double.size() === 4)
  }

  test("nodeAt"){
    var double = new DoubleLinkedList()
    double.addFirst(20)
    double.addFirst(30)
    double.addLast(40)
    double.addFirst(50)
    assert(double.nodeAt(1).data === 30)
    assert(double.removeFirst() === 50)
    assert(double.nodeAt(0).data === 30)
  }

  test("insertAt"){
    var double = new DoubleLinkedList()
    double.addFirst(20)
    double.addFirst(30)
    double.addLast(40)
    double.addFirst(50)
    assert(double.toArray === Array(50,30,20,40))

  }


}
