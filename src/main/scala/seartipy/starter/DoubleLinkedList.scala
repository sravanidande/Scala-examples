package seartipy.starter

class DoubleNode(var data: Int,var prev:DoubleNode=null,var next:DoubleNode = null)

 class DoubleLinkedList {
   private var head: DoubleNode = null
   private var tail: DoubleNode = null
   private var length: Int = 0

   def first(): Int = head.data

   def last(): Int = tail.data

   def size(): Int = length

   def addFirst(value: Int): Unit = {
     val node = new DoubleNode(value, next=head, prev=null)
     var first = head
     if (first == null) {
       tail = node
       head = node
     }
     else {
       head.prev = node
       head = node
     }
     length += 1
   }


   def addLast(value: Int): Unit = {
     var node = new DoubleNode(value, next=null, prev =tail)
     var last = tail
     if (last != null) {
       last.next = node
       tail = node
     }
     else {
       head = node
       tail = node
     }
     length += 1
   }


   def toArray:Array[Int]= {
     var current = head
     val arr = new Array[Int](length)
     var i = 0
     while (current != null) {
       arr(i) = current.data
       current = current.next
       i += 1
     }
     arr
  }



  def findAt(value:Int):DoubleNode = {
    var first = head
    while (first != null) {
      if (first.data == value) {
        return first
      }
      first = first.next
    }
    null
  }


   def nodeAt(index:Int):DoubleNode = {
     var first = head
     var i = 0
     while(first != null){
       if(i == index){
         return first
       }
       first = first.next
       i+=1
     }
     null
   }


   def removeFirst():Int = {
     require(head != null)

     var result = head.data
     if(head.next != null){
        head.next.prev = null
     }
     head = head.next

     length -= 1
     result
   }

   def removeLast():Int = {
     require(tail != null)

     var result = tail.data
     if(tail.prev != null){
       tail.prev.next = null
     }
     tail = tail.prev

     length -= 1
     result
   }




   def insertAt(value:Int,at:DoubleNode):Unit = {
   val node = new DoubleNode(value,prev = at.prev,next = at)
     if(at == head){
       head = node
       if(at.prev != null)
         at.prev.next = node
       at.prev = node
       length += 1
     }
   }




}
