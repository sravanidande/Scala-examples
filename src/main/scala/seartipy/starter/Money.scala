package seartipy.starter

class Money(_dollar:Int,_cent:Int){
  private def this(totalcents:Int) = this(totalcents/100,totalcents%100)

  var totalcents = _dollar*100 + _cent
  var dollar:Int = totalcents/100
  var cent:Int = totalcents%100

  def +(that:Money):Money = new Money(totalcents + that.totalcents)
  def -(that:Money):Money = new Money(totalcents - that.totalcents)
  def *(n:Int):Money = new Money(n*totalcents)
  def /(n:Int):Money = new Money(n/totalcents)

  def compareTo(that:Money):Int = totalcents - that.totalcents


}
