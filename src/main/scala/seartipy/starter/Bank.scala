package seartipy.starter

class Bank {
  private var customers = Map.empty[Int, Customer]
  private var accounts = Map.empty[Int, Account]
  private var nextCutstomerId: Int = 0
  private var nextAccountId: Int = 0

  def createCustomer(name: String, address: Address): Int = {
    val customer = new Customer(nextCutstomerId, name, address)
    customers = customers + (nextCutstomerId -> customer)
    nextCutstomerId += 1
    customer.id
  }

  def createAccount(CustomerId: Int, initialAmount: Money): Int = {
    val account = new Account(nextAccountId, initialAmount)
    accounts = accounts + (nextAccountId -> account)
    nextAccountId += 1
    account.id
  }

  def checkBalance(id:Int):Money = {
    val account = accounts(id)
    account.getBalance
  }

  def withdraw(id:Int,amount:Money):Boolean = {
    val account = accounts(id)
    account.withdraw(amount)
  }

  def deposit(id:Int,amount:Money):Unit = {
    val account = accounts(id)
    account.deposit(amount)
  }


}