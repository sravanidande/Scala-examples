package seartipy.starter



object Utils{
  def squareAll(v:Vector[Int]):Vector[Int] = {
    for (e <- v) yield e * e
  }

  def evenAll(v:Vector[Int]):Vector[Int] = {
    for{
      e <- v
      if(e % 2 == 0)
    } yield e
  }

  def sumAll(v:Vector[Int]):Int = {
    var sum = 0
    for{
      e <- v
    }
      sum = sum + e
    sum
  }

  def factors(n:Int):Vector[Int] = {
    for{
      e <- n
      if n % e == 0
    }yield e
  }

  def isPrime(n:Int):Boolean = {
    var s = factors(n)
    s.size == 2
  }

  def isPerfect(n:Int):Boolean = {
    var sum = 0
    for{
      e <- n
      if n % e == 0
    }
      sum = sum + e
    sum == n
  }

  def generatePrime(r:Int) = {
    Range(0,r).filter(isPrime)
  }
  

}



