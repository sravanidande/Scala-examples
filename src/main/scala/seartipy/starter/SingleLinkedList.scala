package seartipy.starter

class SingleNode(var data:Int,var next:SingleNode)

class SingleLinkedList {
  private var head: SingleNode = null
  private var length: Int = 0

  def first: Int = head.data

  def size: Int = length

  def addFirst(value: Int): Unit = {
    val node = new SingleNode(value, head)
    node.next = head
    head = node
    length += 1
  }

  def removeFirst(): Int = {
    var result = head.data
    head = head.next
    length -= 1
    result
  }

  def nodeAt(index:Int):SingleNode = {
    var i = 0
    var temp = head
    while(temp != null) {
      if (i == index) {
        return temp
      }
      temp = temp.next
      i += 1
    }
    null
  }

  def toArray:Array[Int]= {
    var current = head
    val arr = new Array[Int](length)
    var i = 0
    while (current != null) {
      arr(i) = current.data
      current = current.next
      i += 1
    }
    arr
  }



  def insertAfter(value: Int, node: SingleNode):Unit = {
    require(node != null)
    var newNode = new SingleNode(value, node.next)
    node.next = newNode
    length += 1
  }

  def removeAfter(node:SingleNode):Int = {
    node.next = node.next.next
    val result = node.data
    length -= 1
    result
  }

}


