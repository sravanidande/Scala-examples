package seartipy.starter

class Complex(val real: Rational, val imaginary: Rational) {

  def +(that: Complex): Complex =
    new Complex(real + that.real, imaginary + that.imaginary)

  def -(that: Complex): Complex =
    new Complex(real - that.real, imaginary - that.imaginary)

  def *(that: Complex): Complex =
    new Complex(real * that.real, imaginary * that.imaginary)

  def /(that: Complex): Complex = {
    val d = that.real * that.real - that.imaginary * that.imaginary
    val num = (real * that.real + imaginary * that.imaginary) / d
    val den = (imaginary * that.real - real * that.imaginary) / d
    new Complex(num, den)

  }
}
