package seartipy.starter

object Main extends App {
  println("hello")

  def min(x:Int,y:Int):Int = if(x<y) x else y


  def gcd(x:Int,y:Int): Int = {
    var p = min(x, y)
    if(p == 0) {
      return 1
    }
    while (true) {
      if (x % p == 0 && y % p == 0) {
        return p
      }
      p -= 1
    }
    -1
  }
}

