package seartipy.starter

class Account(val id:Int, var balance:Money) {
  def withdraw(amount:Money):Boolean = {
    if(amount.compareTo(balance)<=0){
      balance -= amount
      true
    }
    else{
       false
    }
  }

  def deposit(amount:Money):Unit = {
    balance += amount
  }

  def getBalance:Money = balance

}
