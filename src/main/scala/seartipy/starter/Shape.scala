package seartipy.starter

sealed trait Shape

case class Rectangle( width:Int, height:Int) extends Shape
case class Circle( radius:Int) extends Shape

object ShapeUtils {
  def area(shape:Shape):Double =  shape match {
    case Rectangle(width,height) => width*height
    case Circle(radius) => Math.PI * radius * radius
  }

  def perimeter(shape:Shape):Double = shape match {
    case Rectangle(width,height) => 2*(width + height)
    case Circle(radius) => 2*Math.PI*radius
  }
}
