# Scala Starter

Scala development using [sbt]() build tool.

## Usage

1. Install [JDK](http://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html), [sbt](http://www.scala-sbt.org). If you are on mac or ubuntu, you could use the following command to install both JDK and sbt. It's from my [lean-dotfiles](https://gitlab.com/seartipy/lean-dotfiles) configuration.

    On Mac

        curl -L j.mp/srtpldf > setup && bash setup nodefaults scala

    On Ubuntu

        wget -qO- j.mp/srtpldf > setup && bash setup nodefaults scala

You could also install and configure Emacs by adding option `emacs`. Drop `nodefaults` if you want an awesome prompt in zsh and bash. For examples to install scala and emacs with an awesome prompt, you could

        curl -L j.mp/srtpldf > setup && bash setup scala emacs      # MacOS
        wget -qO- j.mp/srtpldf > setup && bash setup scala emacs    # Ubuntu

2. Clone this repository and install npm packages. Make sure you have [git](https://git-scm.com/) installed.

        git clone https://gitlab.com/pervezfunctor/scala-starter.git
        cd node-starter

Now you could import this project in Intellij Idea.

For eclipse, execute the following command

        sbt eclipse

And open the project in Eclipse

For emacs, execute the following command

        sbt ensimeConfig

Now in emacs, open src/main/scala/Main.scala and type `M-x ensime` and hit enter. Once ensime starts, you should get autocompletion and on the fly error checking.

## License

MIT
